<?php
	session_start();

	if (!isset($_SESSION['access_token'])) {
		header('Location: login.php');
		exit();
	}
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Bienvenue</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">
</head>
<body>
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = 'https://connect.facebook.net/fr_FR/sdk.js#xfbml=1&version=v2.11&appId=353624495107626';
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
<div id="fb-root"></div>


<div class="container" style="margin-top: 100px">
		<div class="row justify-content-center">
			<div class="col-md-3">
				<img src="<?php echo $_SESSION['userData']['picture']['url'] ?>">
			</div>




  
			<div class="col-md-9">
				<table class="table table-hover table-bordered">
					<tbody>
						<tr>
							<td>ID</td>
							<td><?php echo $_SESSION['userData']['id'] ?></td>
						</tr>
						<tr>
							<td>Nom</td>
							<td><?php echo $_SESSION['userData']['first_name'] ?></td>
						</tr>
						<tr>
							<td>Prenom</td>
							<td><?php echo $_SESSION['userData']['last_name'] ?></td>
						</tr>
						<tr>
							<td>Email</td>
							<td><?php echo $_SESSION['userData']['email'] ?></td>
						</tr>
					</tbody>
				</table>
				
			</div>
		</div>
		
		
		<table class="table table-hover table-bordered">
					<tbody>
					
					
					<tr>
							
							<td  align="center"><div class="fb-page" data-href="https://www.facebook.com/Tunisian.Youth.2012/" data-tabs="timeline" data-small-header="true" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><blockquote cite="https://www.facebook.com/Tunisian.Youth.2012/" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/Tunisian.Youth.2012/">‎الشباب المسلم‎</a></blockquote></div></td>
						</tr>
					
					<tr>
							
							<td  align="center"><iframe src="https://www.facebook.com/plugins/post.php?href=https%3A%2F%2Fwww.facebook.com%2Fpermalink.php%3Fstory_fbid%3D588929101440569%26id%3D570702536596559%26substory_index%3D1&width=500" width="500" height="300" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true"></iframe></td>
						</tr>
						<tr>
							
							<td  align="center"><div class="fb-video" data-href="https://www.facebook.com/jemmi.khaled/videos/10204958095608747/" data-width="500" data-show-text="false"><blockquote cite="https://www.facebook.com/jemmi.khaled/videos/10204958095608747/" class="fb-xfbml-parse-ignore"></blockquote></div></td>
						</tr>
						<tr>
							
							<td  align="center"><div class="fb-save"></div>
						</tr>
						<tr>
							
							<td  align="center"><div class="fb-like" data-href="https://www.facebook.com/Projet-1-mastere-MPIW-Community-570702536596559/?ref=settings" data-layout="standard" data-action="like" data-size="small" data-show-faces="true" data-share="true"></div><br/><br/><br/>
<div class="fb-comments" data-href="https://www.facebook.com/permalink.php?story_fbid=588926084774204&id=570702536596559&comment_id=588927361440743&notif_id=1512596741547248&notif_t=feed_comment" data-numposts="3"></div>
	</div></td>
						</tr>
						</table>
		
		<!-- Your like button code -->

	

<h2><a href = "deconnection.php">deconnection</a></h2>	
</body>
</html>